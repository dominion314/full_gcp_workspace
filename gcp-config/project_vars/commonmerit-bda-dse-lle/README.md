# commonmerit-bda-dse-lle

Configuration for Google Project 'commonmerit-bda-dse-lle'

## Structure

```bash
├── project.yml # GCP project configuration
├── hierarchy.lst # List of directories to process
├── validation_manifest.json # Configuration file validation
├── default-vars # Labels to be applied to all objects that can be labelled.
│   ├── readme.md
│   ├── validation_manifest.json
│   ├── File1.yml
│   ├── File2.yml
│   ├── File3.yml
│   └── ...
└── buckets # Storage bucket configuration files
    ├── readme.md
    ├── validation_manifest.json
    ├── File1.yml
    ├── File2.yml
    ├── File3.yml
    └── ...
```
