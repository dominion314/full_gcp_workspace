# commonmerit-cpe-sample-lle

Configuration for Google Project 'commonmerit-cpe-sample-lle'

## Structure

```bash
├── project.yml # GCP project configuration
├── hierarchy.lst # List of directories to process
├── default-vars # Labels to be applied to all objects that can be labelled.
│   ├── readme.md
│   ├── File1.yml
│   ├── File2.yml
│   ├── File3.yml
│   └── ...
└── buckets # Storage bucket configuration files
    ├── readme.md
    ├── File1.yml
    ├── File2.yml
    ├── File3.yml
    └── ....
```
