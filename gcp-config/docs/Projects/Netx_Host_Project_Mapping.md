

| Product Domains                               | Domain Leader    | Associated Host Projects |                        | Notes                                         |
|-----------------------------------------------|------------------|:------------------------:|:----------------------:|-----------------------------------------------|
|                                               |                  |           PROD           |        NON-PROD        |                                               |
| Enterprise Services, Credit & Payments        | Tony Albergo     | commonmerit-eta-xpn-prd        | commonmerit-eta-xpn-lle      |                                               |
| Merchcandising, Marketing, Analytics          | Niki Allen       | commonmerit-merch-xpn-prd      | commonmerit-merch-xpn-lle**  | primary                                       |
|                                               |                  | commonmerit-mkt-xpn-prd        | commonmerit-mkt-xpn-lle      | overflow                                      |
| Supply Chain, Field Support & People Services | Scott Vifquain   | commonmerit-seed-xpn-prd       | commonmerit-seed-xpn-lle     |                                               |
| Connected Customer Experience                 | Siobhan McFeeney | commonmerit-cx-xpn-prd         | commonmerit-cx-xpn-lle       |                                               |
| Architecture                                   | Jason Fei        | commonmerit-ea-xpn-prd         | commonmerit-ea-xpn-lle       | EA doesn't have a production project          |
| Security                                      | Becky Janutis    | commonmerit-sec-xpn-prd        | commonmerit-sec-xpn-lle      |                                               |
| Infra & Ops                                   | Ritch Houdek     | commonmerit-cpe-xpn-prd        | commonmerit-cpe-xpn-lle      | Resources belonging to Cloud Platform teams   |
|                                               |                  | commonmerit-infra-xpn-prd      | commonmerit-infra-xpn-lle    | Resources belonging to Infrastructure teams   |
| The Platform                                  | Ritch Houdek     | commonmerit-platform-xpn-prd   | commonmerit-platform-xpn-lle | Shared platform for customers containing XaaS |
 
 **=pending
