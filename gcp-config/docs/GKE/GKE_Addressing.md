# GKE Addressing

At this time, commonmerit Engineering only supports RFC-1918 addresses for Node, Pod, Service and Master ranges. Follow the [Google Cloud documentation](https://cloud.google.com/kubernetes-engine/docs/how-to/alias-ips#defaults_limits) for further guidance on sizing.